# Бот для публикации контента в разные каналы

## Запуск приложения
```
cp .env.example .env
docker compose up
```

## Миграции
```
docker compose run --rm bot_client alembic upgrade head
```

## Линтеры
```
docker compose run --rm bot_client make lint
```