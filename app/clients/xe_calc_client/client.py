import logging

from aiohttp import ClientError, ClientResponseError
from pydantic import ValidationError
from yarl import URL

from clients.base_http_client import BaseHttpClient
from clients.xe_calc_client.dto import XeCalcProduct


class XeCalcClientError(Exception):
    pass


class XeCalcClient(BaseHttpClient):
    DOMAIN = URL("https://api.xecalc.ru/api/products/")

    async def get_portions(self, product_slug: str) -> XeCalcProduct:
        try:
            async with self.client.get(self.DOMAIN / product_slug / "product-with-portions/") as response:
                return XeCalcProduct(**(await response.json()))
        except ClientResponseError as e:
            logging.error(f"При получении информации о продукте {product_slug} произошли ошибки - {e}")
        except ClientError:
            logging.error(f"При получении информации о продукте {product_slug} произошли ошибки")
        except ValidationError as e:
            logging.error(f"При валидации продукта {product_slug} произошли ошибки {e}")
        raise XeCalcClientError
