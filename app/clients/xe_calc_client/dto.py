from pydantic import BaseModel, Field


class XeCalcProductBrand(BaseModel):
    slug: str
    name: str


class XeCalcPortion(BaseModel):
    title: str = Field(alias="name")
    calories: float
    fat: float
    protein: float
    carbohydrates: float
    xe_count: float


class XeCalcProduct(BaseModel):
    name: str
    brand: XeCalcProductBrand | None
    portions: list[XeCalcPortion]
