from typing import Self

from aiohttp import ClientSession


class BaseHttpClient:
    client: ClientSession
    raise_for_status: bool = True

    def setup_client(self) -> ClientSession:
        return ClientSession(raise_for_status=self.raise_for_status)

    async def __aenter__(self) -> Self:
        self.client = self.setup_client()
        return self

    async def __aexit__(
        self,
        exc_type: BaseException | None,
        exc_val: BaseException | None,
        exc_tb: BaseException | None,
    ) -> None:
        await self.client.close()
        return None
