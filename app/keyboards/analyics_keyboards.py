from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup


def build_cancel_analytic_button() -> InlineKeyboardMarkup:
    return InlineKeyboardMarkup().add(InlineKeyboardButton(text="Отмена", callback_data="analytics_cancel"))


def build_list_item_controllers() -> InlineKeyboardMarkup:
    keyboard = InlineKeyboardMarkup()
    keyboard.row(
        InlineKeyboardButton(text="Загруженная аналитика", callback_data="publisher_menu_analytics_list"),
        InlineKeyboardButton(text="Загрузить аналитику", callback_data="publisher_menu_analytics_load"),
    )
    keyboard.add(InlineKeyboardButton(text="Назад", callback_data="publisher_menu_back"))
    return keyboard
