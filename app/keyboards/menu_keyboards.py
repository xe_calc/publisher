from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup


def build_entry_menu() -> InlineKeyboardMarkup:
    buttons = [
        InlineKeyboardMarkup(text="Планировщик контента", callback_data="publisher_menu_content_planing"),
        InlineKeyboardMarkup(text="Аналитика", callback_data="publisher_menu_analytics"),
    ]
    keyboard = InlineKeyboardMarkup(row_width=2)
    keyboard.add(*buttons)
    return keyboard


def build_planner_menu() -> InlineKeyboardMarkup:
    keyboard = InlineKeyboardMarkup(row_width=2)
    return keyboard


def build_analytics_menu() -> InlineKeyboardMarkup:
    # TODO: Избавится от импортов
    from handlers.statistic import StatListView

    keyboard = InlineKeyboardMarkup()
    keyboard.row(
        InlineKeyboardButton(text="Загруженная аналитика", callback_data=StatListView.build_initial_callback()),
        InlineKeyboardButton(text="Загрузить аналитику", callback_data="publisher_menu_analytics_load"),
    )
    keyboard.add(InlineKeyboardButton(text="Назад", callback_data="publisher_menu_back"))
    return keyboard


def build_content_plan_menu() -> InlineKeyboardMarkup:
    # TODO: Избавится от импортов
    from handlers.content_plan import (
        PreparedContentAdminView,
        TargetListView,
        XeCalcPostView,
    )

    buttons = [
        InlineKeyboardMarkup(text="Площадки (цели) публикаций", callback_data=TargetListView.build_initial_callback()),
        InlineKeyboardMarkup(
            text="Продукты и порции (xecalc.ru)", callback_data=XeCalcPostView.build_initial_callback()
        ),
        InlineKeyboardMarkup(
            text="Подготовленный контент", callback_data=PreparedContentAdminView.build_initial_callback()
        ),
    ]
    keyboard = InlineKeyboardMarkup(row_width=2)
    keyboard.add(*buttons)
    keyboard.add(InlineKeyboardButton(text="Назад", callback_data="publisher_menu_back"))
    return keyboard
