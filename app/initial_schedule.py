import asyncio
from datetime import time

from models.content_plan import TargetSystem, WeekDays
from models.db import async_session_factory
from services.content_plan_service import ContentPlanService, UniqueError
from services.validators import CycleSlotDTO, TargetDTO, TGCredentials


def initial_xe_calc_telegram_target(chat_id: str, tg_bot_token: str) -> TargetDTO:
    return TargetDTO(
        title="XeCalc Канал",
        system_slug=TargetSystem.TELEGRAM,
        credentials=TGCredentials(bot_token=tg_bot_token, chanel_id=chat_id),
    )


def xe_calc_time_slots() -> tuple:
    return (
        (WeekDays.MONDAY, time(hour=12)),
        (WeekDays.MONDAY, time(hour=18)),
        (WeekDays.TUESDAY, time(hour=12)),
        (WeekDays.TUESDAY, time(hour=18)),
        (WeekDays.WEDNESDAY, time(hour=12)),
        (WeekDays.WEDNESDAY, time(hour=18)),
        (WeekDays.THURSDAY, time(hour=12)),
        (WeekDays.THURSDAY, time(hour=18)),
        (WeekDays.FRIDAY, time(hour=12)),
        (WeekDays.FRIDAY, time(hour=18)),
        (WeekDays.SATURDAY, time(hour=12)),
        (WeekDays.SATURDAY, time(hour=18)),
        (WeekDays.SUNDAY, time(hour=12)),
        (WeekDays.SUNDAY, time(hour=18)),
    )


async def setup_xe_calc_content_slots(data: TargetDTO) -> None:
    async with async_session_factory() as session:
        service = ContentPlanService(session)
        target = await service.create_or_update(data)
        for week_day, pub_time in xe_calc_time_slots():
            slot_data = CycleSlotDTO(target_id=target.id, weekly_day=week_day, time_of_day=pub_time)
            try:
                await service.create_slot(CycleSlotDTO(target_id=target.id, weekly_day=week_day, time_of_day=pub_time))
            except UniqueError:
                print("Слот уже занят", slot_data)


if __name__ == "__main__":
    chanel_id = input("Введите id канала")
    bot_token = input("Введите токен")
    data_target = TargetDTO(
        title="XeCalc Канал",
        system_slug=TargetSystem.TELEGRAM,
        credentials=TGCredentials(bot_token=bot_token, chanel_id=chanel_id),
    )
    asyncio.run(setup_xe_calc_content_slots(data_target))
