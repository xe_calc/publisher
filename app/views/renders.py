from dataclasses import dataclass
from enum import Enum
from typing import Any, Callable


@dataclass
class ListObjects:
    data: list[str]


class AnyToStrRender:
    """Преобразует разные типы в читаемую строку"""

    render_registry: dict[type, Callable] = {}

    @classmethod
    def register(cls, input_type: type) -> Callable:
        def wrapper(render_function: Callable) -> Callable:
            if input_type not in cls.render_registry:
                cls.render_registry[input_type] = render_function
            return render_function

        return wrapper

    @classmethod
    def render(cls, value: Any) -> str:
        input_type = type(value)
        if input_type == str:
            return value
        for render_type, render in cls.render_registry.items():
            if isinstance(value, render_type):
                return render(value)
        return ""


@AnyToStrRender.register(bool)
def bool_to_str(val: bool) -> str:
    return "Да" if val else "Нет"


@AnyToStrRender.register(int)
def int_to_str(val: int) -> str:
    return str(val)


@AnyToStrRender.register(Enum)
def enum_to_str(val: Enum) -> str:
    return val.name


@AnyToStrRender.register(ListObjects)
def list_objects_to_str(val: ListObjects) -> str:
    if val.data:
        return "\n".join([value for value in val.data])
    return "Пусто"


@AnyToStrRender.register(dict)
def dict_to_str(val: dict) -> str:
    return str(val)
