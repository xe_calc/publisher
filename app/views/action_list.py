from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.utils.callback_data import CallbackData

from views.dto import Action


class ActionListViewBuilder:
    """
    Представление экшенов в виде кнопок
    """

    callback_data: CallbackData
    actions_list: list[Action]
    cancel_callback: str

    def __init__(self, actions_list: list[Action], cancel_callback: str):
        self.actions_list = actions_list
        self.cancel_callback = cancel_callback

    def build_keyboard(self) -> InlineKeyboardMarkup:
        """
        Собирает клавиатуру с экшенами
        """
        inline_keyboard = InlineKeyboardMarkup()
        for action in self.actions_list:
            inline_keyboard.add(
                InlineKeyboardButton(
                    action.description, callback_data=self.callback_data.new(controller="call_action", data=action.slug)
                )
            )
        inline_keyboard.row(InlineKeyboardButton("Назад", callback_data=self.cancel_callback))
        return inline_keyboard
