from dataclasses import dataclass


@dataclass
class Action:
    slug: str
    description: str


@dataclass
class AdminSettings:
    list_fields: list[str]
    detail_scheme: list[tuple[str, str] | str]
    list_items_per_page: int = 10
    key_for_detail_link: str = "id"


@dataclass
class DetailField:
    field: str
    value: str
    description: str | None
