import logging

from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.utils.callback_data import CallbackData

from models.db import Base
from views.dto import Action, AdminSettings, DetailField
from views.renders import AnyToStrRender


class DetailViewBuilder:
    """
    Детальное представление объекта модели

    Атрибуты:
        obj - модель.

        model_title - заголовок модели.

        callback_data - Фабрика данных для колбэков

        list_page - Последняя страница в списке (нужно для возврата в нужную страницу списка)

        admin_settings - Настройки отображения

        actions - Доступные экшены
    """

    obj: Base
    model_title: str
    callback_data: CallbackData
    list_page: int
    admin_settings: AdminSettings
    actions: list[Action]

    def __init__(
        self,
        obj: Base,
        model_title: str,
        callback_data: CallbackData,
        admin_settings: AdminSettings,
        actions: list[Action],
        list_page: int,
    ):
        self.obj = obj
        self.model_title = model_title
        self.callback_data = callback_data
        self.admin_settings = admin_settings
        self.actions = actions
        self.list_page = list_page

    def build_body(self) -> str:
        # Сборка тела сообщения
        return f"Детальная информация - {self.model_title}\n{self._render_body()}"

    def _build_fields(self) -> list[DetailField]:
        fields_body: list[DetailField] = []
        for field in self.admin_settings.detail_scheme:
            if isinstance(field, tuple):
                db_field = field[0]
                description = field[1]
            elif isinstance(field, str):
                db_field = field
                description = None
            else:
                continue
            fields_body.append(
                DetailField(
                    field=db_field,
                    value=AnyToStrRender.render(getattr(self.obj, db_field, "")),
                    description=description,
                )
            )
        return fields_body

    def _render_body(self) -> str:
        result = ""
        for field in self._build_fields():
            result += f"{field.field} = {field.value}"
            if description := field.description:
                result += f" ({description})\n"
            else:
                result += "\n"
        return result

    def build_keyboard(self) -> InlineKeyboardMarkup:
        """
        Собирает клавиатуру с экшенами
        """
        inline_keyboard = InlineKeyboardMarkup()
        for action in self.actions:
            logging.info(action.description)
            inline_keyboard.add(
                InlineKeyboardButton(
                    action.description,
                    callback_data=self.callback_data.new(
                        controller="call_action", data=f"{action.slug}|{self.obj.id}|{self.list_page}"
                    ),
                )
            )
        inline_keyboard.row(
            InlineKeyboardButton(
                "Назад", callback_data=self.callback_data.new(controller="navigate_list", data=self.list_page)
            )
        )
        return inline_keyboard
