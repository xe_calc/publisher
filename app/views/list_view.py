import math
from functools import cached_property
from typing import Sequence

from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.utils.callback_data import CallbackData
from sqlalchemy import Row

from views.dto import AdminSettings


class ListViewBuilder:
    """
    Класс для построения списочного сообщения.
    Для построения тела сообщения используется метод build_body, для построения кнопок - build_keyboard.

    Атрибуты:
        items - Список объектов.

        all_items_count - Всего объектов.

        current_page - Текущая страница

        key_for_detail_link - Ключ для линка детального отображения

        callback_data - Фабрика данных для колбэков

        settings - Настройки отображения

        cancel_callback_data - колбэк для отмены

        border - Разделитель между полями

        label_next - Текст для кнопки вперед

        label_prev - текст для кнопки назад

        pages_per_row - количество ссылок на страницы в одной линии

        key_links_per_row - количество ссылок на детальную страницу в одной линии
    """

    items: Sequence[Row]
    all_items_count: int
    current_page: int
    key_for_detail_link: str
    callback_data: CallbackData
    settings: AdminSettings
    cancel_callback_data: str
    border: str = "|"
    label_next: str = ">>>"
    label_prev: str = "<<<"
    pages_per_row: int = 8
    key_links_per_row: int = 5

    def __init__(
        self,
        items: Sequence[Row],
        all_items_count: int,
        current_page: int,
        settings: AdminSettings,
        callback_data: CallbackData,
        cancel_callback_data: str,
    ):
        self.current_page = current_page
        self.settings = settings
        self.callback_data = callback_data
        self.cancel_callback_data = cancel_callback_data
        self.all_items_count = all_items_count
        self.items = items

    def build_body(self) -> str:
        """
        Собирает тело сообщения.
        """
        return f"{self._render_header(self._all_pages)}\n\n{self._render_body()}"

    def build_keyboard(self) -> InlineKeyboardMarkup:
        """
        Собирает клавиатуру
        """
        inline_keyboard = InlineKeyboardMarkup()
        self._build_detail_links(inline_keyboard)
        self._build_navigation_by_list_row(inline_keyboard)
        inline_keyboard.row(
            InlineKeyboardButton("Действия", callback_data=self.callback_data.new(controller="show_actions", data=""))
        )
        self._build_pages_list(inline_keyboard)
        inline_keyboard.row(InlineKeyboardButton("Отмена", callback_data=self.cancel_callback_data))
        return inline_keyboard

    def _render_body(self) -> str:
        # Построение тела сообщения
        prepared_items: list[dict] = []
        for row in self.items:
            prepared_items.append({field: getattr(row, field) for field in self.settings.list_fields})
        body = "\n".join([self._render_item_line(fields) for fields in prepared_items])
        return body

    def _render_item_line(self, fields: dict) -> str:
        # Построение элемента списка
        border = self.border
        return f"{border} {', '.join([f'{name}: {value}' for name, value in fields.items()])} {border} \n"

    @cached_property
    def _all_pages(self) -> int:
        # Количество страниц
        return math.ceil(self.all_items_count / self.settings.list_items_per_page)

    def _render_header(self, all_pages: int) -> str:
        # Построение 'шапки' списка
        return f"Страница {self.current_page} из {all_pages}. Всего записей {self.all_items_count}"

    def _build_detail_links(self, markup: InlineKeyboardMarkup) -> None:
        # Сборка кнопок для перехода к детальному виду
        if self.items:
            keys = [str(getattr(row, self.settings.key_for_detail_link, "N/A")) for row in self.items]
            _keys_list = keys.copy()
            while len(_keys_list) != 0:
                markup.row(
                    *[
                        InlineKeyboardButton(
                            f"#{key}",
                            callback_data=self.callback_data.new(
                                controller="detail", data=f"{key}|{self.current_page}"
                            ),
                        )
                        for key in _keys_list[: self.key_links_per_row]
                    ]
                )
                _keys_list = _keys_list[self.key_links_per_row :]

    def _build_navigation_button(self, arrow_way: str | None = None, page: int | None = None) -> InlineKeyboardButton:
        # Построение кнопок навигации и пагинации
        label: str | int
        next_page: int
        if arrow_way:
            if arrow_way == "next":
                label = self.label_next
                next_page = self.current_page + 1
            elif arrow_way == "prev":
                label = self.label_prev
                next_page = self.current_page - 1
            else:
                raise ValueError("Неправильное значение arrow_way, ожидаемые значения - next или prev")
        elif page:
            label = page
            next_page = page
        else:
            raise ValueError("Не указан arrow_way или page")
        return InlineKeyboardButton(
            label, callback_data=self.callback_data.new(controller="navigate_list", data=next_page)
        )

    def _build_navigation_by_list_row(self, markup: InlineKeyboardMarkup) -> None:
        # Построение кнопок пагинации по 'стрелкам'
        has_next = self.current_page < self._all_pages
        has_prev = self._all_pages > 1 and self.current_page > 1

        if has_prev and has_next:
            markup.row(
                *[self._build_navigation_button(arrow_way="prev"), self._build_navigation_button(arrow_way="next")]
            )
        elif has_prev:
            markup.row(*[self._build_navigation_button(arrow_way="prev")])
        elif has_next:
            markup.row(*[self._build_navigation_button(arrow_way="next")])

    def _build_pages_list(self, markup: InlineKeyboardMarkup) -> None:
        # Кнопки пагинации по страницам
        cursor = 1
        for _ in range(math.ceil(self._all_pages / self.pages_per_row)):
            row = []
            for _ in range(self.pages_per_row):
                if cursor <= self._all_pages:
                    row.append(self._build_navigation_button(page=cursor))
                    cursor += 1
            markup.row(*row)
