from pydantic import BaseSettings


class AppConfig(BaseSettings):
    admin_bot_token: str
    postgres_url: str
    media_folder: str


app_config = AppConfig()
