import os
import uuid

from PIL import Image

from config import app_config


class Storage:
    entity: str

    def __init__(self, entity: str) -> None:
        self.entity = entity

    async def save(self, image: Image, filename: str) -> str:
        raise NotImplementedError

    async def open(self, filename: str) -> Image:
        raise NotImplementedError


class LocalStorage(Storage):
    async def save(self, image: Image, filename: str) -> str:
        storage_filename = f"{uuid.uuid4()}_{filename}"
        if not os.path.exists(self.full_path):
            os.makedirs(self.full_path)

        image.save(os.path.join(self.full_path, storage_filename))
        return storage_filename

    async def open(self, filename: str) -> Image:
        return Image.open(os.path.join(self.full_path, filename))

    @property
    def full_path(self) -> str:
        return os.path.join(app_config.media_folder, f"{self.entity}/")
