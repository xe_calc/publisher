from PIL import Image, ImageDraw

from content_builder.elements import Element
from content_builder.storage import Storage


class Builder:
    async def build(self, filename: str, storage: Storage) -> str:
        raise NotImplementedError


class ListBuilderWithBackground(Builder):
    background_path: str
    elements: list[Element]

    def __init__(self, background_path: str) -> None:
        self.background_path = background_path
        self.elements = []

    def add_element(self, element: Element) -> None:
        self.elements.append(element)

    async def build(self, filename: str, storage: Storage) -> str:
        with Image.open(self.background_path) as image:
            layout = ImageDraw.Draw(image)
            for element in self.elements:
                element.overlay(layout)
            return await storage.save(image, filename)
