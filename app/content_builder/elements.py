from dataclasses import dataclass

from PIL.ImageDraw import ImageDraw
from PIL.ImageFont import FreeTypeFont


class Element:
    def overlay(self, layout: ImageDraw) -> None:
        raise NotImplementedError


@dataclass
class Color:
    red: int
    green: int
    blue: int

    def as_tuple(self) -> tuple[int, int, int]:
        return self.red, self.green, self.blue


@dataclass
class Position:
    align_type: str | None
    margin: float | int | None


class TextElement(Element):
    font: FreeTypeFont | None
    text: str
    text_size: int
    color: Color | None
    position: Position
    width: int
    text_height: int

    def __init__(
        self, text: str, position: Position, color: Color | None = None, font: FreeTypeFont | None = None
    ) -> None:
        self.text = text
        self.color = color
        self.font = font
        self.position = position

    def overlay(self, layout: ImageDraw) -> None:
        if self.color:
            color = self.color.as_tuple()
        else:
            color = (255, 0, 0)
        width, height = layout.im.size
        if self.position.align_type == "x_center":
            xy = (width / 2, self.position.margin)
        elif self.position.align_type == "y_center":
            xy = (self.position.margin, height / 2)
        else:
            xy = (width / 2, height / 2)
        layout.text(xy, text=self.text, fill=color, font=self.font, anchor="mm")
