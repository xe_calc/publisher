import os
from collections import OrderedDict
from dataclasses import dataclass
from typing import Self

from PIL import ImageFont
from slugify import slugify

from content_builder.builders import ListBuilderWithBackground
from content_builder.elements import Color, Position, TextElement
from content_builder.storage import LocalStorage
from models import ProductPostPortion


@dataclass
class BuilderXeCalcPortionPost:
    title: str
    slug: str
    calories: float
    fat: float
    protein: float
    carbohydrates: float
    xe_count: float

    @classmethod
    def create_from_portion(cls, portion: ProductPostPortion) -> Self:
        return cls(
            title=portion.title,
            slug=slugify(portion.title),
            calories=portion.calories,
            fat=portion.fat,
            protein=portion.protein,
            carbohydrates=portion.carbohydrates,
            xe_count=portion.xe_count,
        )


def build_product_param(name: str, value: str, font: ImageFont, max_width: int = 600, delimiter: str = ".") -> str:
    del_size = font.getlength(delimiter)
    name_size = font.getlength(name)
    value_size = font.getlength(value)
    delimiter_count = round((max_width - (name_size + value_size)) / del_size)
    return f"{name}{delimiter * delimiter_count}{value}"


async def build_post(portion_post: BuilderXeCalcPortionPost) -> str:
    current_dir = os.path.dirname(__file__)

    builder = ListBuilderWithBackground(os.path.join(current_dir, "asset/background.png"))
    head_font = ImageFont.truetype(os.path.join(current_dir, "asset/TeXGyreAdventor-Regular.ttf"), 38)
    params_font = ImageFont.truetype(os.path.join(current_dir, "asset/TeXGyreAdventor-Regular.ttf"), 30)

    color = Color(0, 0, 0)
    current_margin = 270

    builder.add_element(
        TextElement(
            text=portion_post.title,
            position=Position(align_type="x_center", margin=current_margin),
            font=head_font,
            color=color,
        )
    )

    params_list = OrderedDict(
        [
            ("calories", "Калории"),
            ("carbohydrates", "Углеводы"),
            ("protein", "Белки"),
            ("fat", "Жиры"),
            ("xe_count", "XE"),
        ]
    )

    for index, (param, name) in enumerate(params_list.items()):
        if index == 0:
            current_margin += 150
        else:
            current_margin += 100
        builder.add_element(
            TextElement(
                text=build_product_param(name, "{:.1f}".format(getattr(portion_post, param)), params_font),
                position=Position(align_type="x_center", margin=current_margin),
                font=params_font,
                color=Color(0, 0, 0),
            )
        )

    return await builder.build(f"{portion_post.slug}.png", LocalStorage(entity="xe_calc_portion_post"))
