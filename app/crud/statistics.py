from sqlalchemy import func, select
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.orm import selectinload

from crud.base import CRUDBase
from crud.base_list_view import BaseAdmin
from models.statistic_item import StatisticItem


class StatisticItemCRUD(CRUDBase):
    async def create_or_update_product(self, url: str, slug: str, count: int, title: str) -> None:
        insert_query = (
            insert(StatisticItem)
            .values(**{"url": url, "slug": slug, "view_count": count, "title": title})
            .on_conflict_do_update(index_elements=[StatisticItem.slug], set_={"view_count": count})
        )
        await self.session.execute(insert_query)
        await self.session.commit()

    async def get_items_count(self) -> int:
        query = select(func.count()).select_from(StatisticItem)
        return await self.session.scalar(query)


class StatisticItemViewCRUD(BaseAdmin):
    model = StatisticItem

    async def get_object_by_id(self, obj_id: int) -> StatisticItem:
        query = select(StatisticItem).where(self.model.id == obj_id).options(selectinload(StatisticItem.post))
        return (await self.session.scalars(query)).one()
