from typing import Any

from sqlalchemy import select
from sqlalchemy.orm import selectinload

from crud.base_list_view import BaseAdmin, CRUDBase
from models.content_plan import Target, TargetSystem


class TargetAdminViewCRUD(BaseAdmin):
    model = Target

    async def get_object_by_id(self, obj_id: int) -> Target:
        query = select(self.model).where(self.model.id == obj_id).options(selectinload(Target.slots))
        return (await self.session.scalars(query)).one()


class TargetCRUD(CRUDBase):
    async def get_by_id(self, target_id: int) -> Target:
        query = select(Target).where(Target.id == target_id)
        return (await self.session.execute(query)).scalar_one()

    async def get_by_fields(self, **fields: Any) -> Target:
        query = select(Target).where(*[getattr(Target, field) == val for field, val in fields.items()])
        return (await self.session.execute(query)).scalar_one()

    async def create(self, title: str, system_slug: TargetSystem, credentials: dict) -> Target:
        target = Target(title=title, system_slug=system_slug, credentials=credentials)
        self.session.add(target)
        await self.session.commit()
        return target

    async def update(self, target_id: int, title: str, system_slug: TargetSystem, credentials: dict) -> Target:
        target = await self.get_by_id(target_id)
        target.title = title
        target.system_slug = system_slug
        target.credentials = credentials

        await self.session.commit()
        return target
