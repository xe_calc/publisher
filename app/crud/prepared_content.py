from dataclasses import asdict
from enum import Enum
from typing import Any

from sqlalchemy import select

from crud.base import CRUDBase
from crud.base_list_view import BaseAdmin
from models.content_plan import PreparedContent
from models.content_types import XeCalcProductPost


def prepare_json_factory(data: Any) -> dict:
    def convert_value(obj: Any) -> Any:
        if isinstance(obj, Enum):
            return obj.value
        return obj

    return dict((k, convert_value(v)) for k, v in data)


class PreparedContentCRUD(CRUDBase):
    async def create_xe_calc_prepared_content(self, target_id: int, product_id: int, data: XeCalcProductPost) -> None:
        self.session.add(
            PreparedContent(
                target_id=target_id, product_post_id=product_id, payload=asdict(data, dict_factory=prepare_json_factory)
            )
        )
        await self.session.commit()

    async def get_prepared_content(self, obj_id: int) -> PreparedContent | None:
        query = select(PreparedContent).where(PreparedContent.id == obj_id)
        return (await self.session.scalars(query)).one_or_none()

    async def add_to_slot(self, target_id: int, slot_id: int) -> None:
        pass


class PreparedContentAdminCRUD(BaseAdmin):
    model = PreparedContent
