import datetime

from sqlalchemy import select

from crud.base import CRUDBase
from models.content_plan import ContentSlot, WeekDays


class ContentSlotCRUD(CRUDBase):
    async def get_by_id(self, slot_id: int) -> ContentSlot:
        query = select(ContentSlot).where(ContentSlot.id == slot_id)
        return (await self.session.execute(query)).scalar_one()

    async def create(
        self,
        target_id: int,
        publish_date: datetime.datetime | None = None,
        weekly_day: WeekDays | None = None,
        time_of_day: datetime.time | None = None,
    ) -> ContentSlot:
        slot = ContentSlot(
            target_id=target_id, publish_date=publish_date, weekly_day=weekly_day, time_of_day=time_of_day
        )
        self.session.add(slot)
        await self.session.commit()
        return slot

    async def update(
        self,
        slot_id: int,
        target_id: int,
        publish_date: datetime.datetime | None,
        weekly_day: WeekDays | None,
        time_of_day: datetime.time | None,
    ) -> ContentSlot:
        slot = await self.get_by_id(slot_id)

        slot.target_id = target_id
        slot.publish_date = publish_date
        slot.weekly_day = weekly_day
        slot.time_of_day = time_of_day

        await self.session.commit()
        return slot
