from sqlalchemy import select
from sqlalchemy.orm import selectinload

from clients.xe_calc_client.dto import XeCalcPortion
from crud.base import CRUDBase
from crud.base_list_view import BaseAdmin
from models.product_post import ProductPost, ProductPostPortion
from models.statistic_item import StatisticItem


class ProductPostCRUD(CRUDBase):
    async def create_from_statistic_item(self, statistic_item: StatisticItem) -> ProductPost:
        product_name = statistic_item.title.split("-")[0].strip()
        product_post = ProductPost(statistic_item_id=statistic_item.id, title=product_name)
        self.session.add(product_post)
        await self.session.commit()
        return product_post

    async def get_for_preparing_content(self, product_id: int) -> ProductPost | None:
        query = (
            select(ProductPost)
            .where((ProductPost.prepared_content is None) & (ProductPost.id == product_id))
            .options(selectinload(ProductPost.portions))
        )
        return (await self.session.scalars(query)).one_or_none()


class ProductPostPortionCRUD(CRUDBase):
    async def create_portions(self, product_post_id: int, raw_portions: list[XeCalcPortion]) -> None:
        portions = [ProductPostPortion(post_id=product_post_id, **raw_portion.dict()) for raw_portion in raw_portions]
        self.session.add_all(portions)
        await self.session.commit()


class ProductPostAdminCRUD(BaseAdmin):
    model = ProductPost

    async def get_object_by_id(self, obj_id: int) -> ProductPost:
        query = (
            select(self.model)
            .where(self.model.id == obj_id)
            .options(selectinload(ProductPost.portions), selectinload(ProductPost.prepared_content))
        )
        return (await self.session.scalars(query)).one()
