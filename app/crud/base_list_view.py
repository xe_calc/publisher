from typing import Sequence

from sqlalchemy import Row, Select, func, select
from sqlalchemy.orm import Mapped

from crud.base import CRUDBase
from models.db import Base


class BaseAdmin(CRUDBase):
    """Базовый набор методов для работы с БД для административного представления"""

    model: type[Base]
    order_by: Mapped = Base.id

    @staticmethod
    def set_limit_offset(select_expression: Select, items_per_page: int, page: int = 0) -> Select:
        select_expression = select_expression.limit(items_per_page).offset(page * items_per_page)
        return select_expression

    async def get_list_items(self, current_page: int, items_per_page: int) -> Sequence[Row]:
        page = current_page - 1
        query = select(self.model)
        query = self.set_limit_offset(query, items_per_page, page).order_by(getattr(self.model, "id"))
        return (await self.session.scalars(query)).all()

    async def get_items_count(self) -> int:
        query = select(func.count()).select_from(self.model)
        return await self.session.scalar(query)

    async def delete_by_id(self, id_list: list[int]) -> None:
        query = select(self.model).where(self.model.id.in_(id_list))
        for user in (await self.session.scalars(query)).all():
            await self.session.delete(user)
        await self.session.commit()

    async def get_object_by_id(self, obj_id: int) -> Base:
        query = select(self.model).where(self.model.id == obj_id)
        return (await self.session.scalars(query)).one()
