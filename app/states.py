from aiogram.dispatcher.filters.state import State, StatesGroup


class LoadStatistic(StatesGroup):
    await_load_file = State()


class AdminStatisticView(StatesGroup):
    list_items_view = State()
    detail_item_view = State()
