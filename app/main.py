import asyncio
import logging

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.redis import RedisStorage2

from config import app_config
from handlers import content_plan, menu_handlers, statistic

logging.basicConfig(level=logging.DEBUG)


async def run_poll() -> None:
    bot = Bot(token=app_config.admin_bot_token)
    dispatcher = Dispatcher(bot=bot, storage=RedisStorage2(host="redis"))
    menu_handlers.setup(dispatcher)
    statistic.setup(dispatcher)
    content_plan.setup(dispatcher)
    await dispatcher.start_polling()


if __name__ == "__main__":
    asyncio.run(run_poll())
