from sqlalchemy.exc import IntegrityError, NoResultFound

from crud.content_slot import ContentSlotCRUD
from crud.content_target import TargetCRUD
from models.content_plan import ContentSlot, Target
from services.base_service import BaseService
from services.validators import CycleSlotDTO, FixedSlotDTO, TargetDTO


class UniqueError(Exception):
    pass


class ContentPlanService(BaseService):
    async def create_or_update(self, data: TargetDTO) -> Target:
        try:
            target = await TargetCRUD(self.session).get_by_fields(
                **{"title": data.title, "system_slug": data.system_slug}  # type: ignore
            )
            return await self.update(target.id, data)
        except NoResultFound:
            return await TargetCRUD(self.session).create(
                title=data.title, system_slug=data.system_slug, credentials=data.credentials.dict()
            )

    async def update(self, target_id: int, data: TargetDTO) -> Target:
        return await TargetCRUD(self.session).update(
            target_id=target_id, title=data.title, system_slug=data.system_slug, credentials=data.credentials.dict()
        )

    async def create_slot(self, data: CycleSlotDTO | FixedSlotDTO) -> ContentSlot:
        try:
            return await ContentSlotCRUD(self.session).create(**data.dict())
        except IntegrityError:
            await self.session.rollback()
            raise UniqueError
