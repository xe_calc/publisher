from clients.xe_calc_client.client import XeCalcClient, XeCalcClientError
from crud.product_post import ProductPostCRUD, ProductPostPortionCRUD
from models.statistic_item import StatisticItem
from services.base_service import BaseService


class LoadProductPostService(BaseService):
    async def load_product_post(self, stat_item: StatisticItem) -> bool:
        async with XeCalcClient() as client:
            try:
                product_info = await client.get_portions(stat_item.slug)
            except XeCalcClientError:
                return False
            else:
                product_post_crud = ProductPostCRUD(self.session)
                product_post_portions_crud = ProductPostPortionCRUD(self.session)
                product_post = await product_post_crud.create_from_statistic_item(statistic_item=stat_item)
                await product_post_portions_crud.create_portions(
                    product_post_id=product_post.id, raw_portions=product_info.portions
                )
                return True
