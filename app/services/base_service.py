from sqlalchemy.ext.asyncio import AsyncSession


class BaseService:
    """
    Базовый класс для определения сервис слоя.
    """

    session: AsyncSession

    def __init__(self, session: AsyncSession):
        """
        :param session: Объект сессии
        """
        self.session = session
