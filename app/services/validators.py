import datetime

from pydantic import BaseModel

from models.content_plan import TargetSystem, WeekDays


class VKCredentials(BaseModel):
    token: str
    group_id: int


class TGCredentials(BaseModel):
    bot_token: str
    chanel_id: str


class TargetDTO(BaseModel):
    title: str
    system_slug: TargetSystem
    credentials: VKCredentials | TGCredentials


class BaseSlotDTO(BaseModel):
    target_id: int


class CycleSlotDTO(BaseSlotDTO):
    weekly_day: WeekDays
    time_of_day: datetime.time


class FixedSlotDTO(BaseSlotDTO):
    publish_date: datetime.datetime
