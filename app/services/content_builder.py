from content_builder.product_portion_card_builder.builder import (
    BuilderXeCalcPortionPost,
    build_post,
)
from crud.content_target import TargetCRUD
from crud.prepared_content import PreparedContentCRUD
from crud.product_post import ProductPostCRUD
from models.content_plan import TargetSystem
from models.content_types import ContentType, XeCalcPortion, XeCalcProductPost
from services.base_service import BaseService


class ContentPrepared(Exception):
    pass


class PrepareContentService(BaseService):
    async def prepare_xe_calc_portion_post(self, product_id: int) -> int:
        prepared_images = 0
        product = await ProductPostCRUD(self.session).get_for_preparing_content(product_id)
        target = await TargetCRUD(self.session).get_by_fields(system_slug=TargetSystem.TELEGRAM, title="XeCalc Канал")
        if product and target:
            portions = []
            for portion in product.portions:
                data = BuilderXeCalcPortionPost.create_from_portion(portion)
                post_image = await build_post(data)
                portions.append(XeCalcPortion(image=post_image))

            await PreparedContentCRUD(self.session).create_xe_calc_prepared_content(
                target.id,
                product_id,
                XeCalcProductPost(
                    product_name=product.title, portions=portions, content_type=ContentType.TG_XE_CALC_PORTION_POST
                ),
            )
        return prepared_images
