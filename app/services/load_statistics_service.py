import csv
from asyncio import gather
from collections import Counter
from dataclasses import dataclass
from io import BytesIO
from urllib.parse import urlparse

from crud.statistics import StatisticItemCRUD
from models.db import async_session_factory


class LoadStatisticServiceException(Exception):
    pass


@dataclass
class RawAnalyticData:
    title: str
    view_count: int
    url: str


class LoadStatisticService:
    valid_headers = {"Заголовок страницы", "Адрес страницы", "Просмотры", "Посетители"}
    MINIMAL_VIEW_COUNT = 100

    async def parse_csv_and_update_products_statistic(self, raw_file: BytesIO) -> tuple:
        csv_reader = csv.DictReader(raw_file.getvalue().decode("utf-8-sig").splitlines())

        if csv_reader.fieldnames and set(csv_reader.fieldnames) != self.valid_headers:
            raise LoadStatisticServiceException(
                f"Не валидные заголовки, ожидаемые заголовки - {', '.join(self.valid_headers)}"
            )

        errors_count = 0
        ignored_count = 0
        success_count = 0
        tasks = []
        for index, row in enumerate(csv_reader, 0):
            if index == 0:
                continue
            tasks.append(
                self.csv_row_handler(
                    RawAnalyticData(
                        title=row["Заголовок страницы"], view_count=int(row["Просмотры"]), url=row["Адрес страницы"]
                    )
                )
            )
        results = Counter(await gather(*tasks, return_exceptions=True))
        for result, count in results.items():
            if isinstance(result, bool):
                if result:
                    success_count = count
                else:
                    ignored_count = count
            if isinstance(result, Exception):
                errors_count = count
        return success_count, ignored_count, errors_count

    async def csv_row_handler(self, raw_data: RawAnalyticData) -> bool:
        if raw_data.view_count >= self.MINIMAL_VIEW_COUNT:
            parse_url = urlparse(raw_data.url)
            split_path = parse_url.path.strip("/").split("/")

            if len(split_path) == 2 and split_path[0] == "products":
                async with async_session_factory() as session:
                    await StatisticItemCRUD(session).create_or_update_product(
                        raw_data.url, split_path[1], raw_data.view_count, raw_data.title
                    )
                return True
        return False
