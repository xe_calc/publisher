import logging
from typing import Callable

from aiogram import Dispatcher, types
from aiogram.types import Message
from aiogram.utils.callback_data import CallbackData

from crud.base import CRUDException
from crud.base_list_view import BaseAdmin
from models.db import async_session_factory
from views.detail_view import DetailViewBuilder
from views.dto import Action, AdminSettings
from views.list_view import ListViewBuilder


def register_action(description: str) -> Callable:
    def decorator(f: Callable) -> Callable:
        f._description = description  # type: ignore [attr-defined]
        return f

    return decorator


class BaseAdminHandler:
    """
    Базовый класс контейнер для работы со списочными данными. Каждая новая сущность в базе данных должна быть описана
    классом который наследует BaseListViewHandler.

    Атрибуты:
        crud_class - класс для работы с источником данных.

        settings - класс настроек для отображения списочных данных.

        list_view_builder - Класс строитель списочного интерфейса

        detail_view_builder - Класс строитель детального интерфейса

        cancel_callback_data - колбэк данные для выхода

        model_title - Заголовок модели

    Точка входа:
        setup - Метод регистрирующий колбэки

    """

    crud_class: type[BaseAdmin]
    settings: AdminSettings
    list_view_builder: type[ListViewBuilder] = ListViewBuilder
    detail_view_builder: type[DetailViewBuilder] = DetailViewBuilder
    model_title: str
    cancel_callback_data: str

    @property
    def callback_data(self) -> CallbackData:
        prefix = self.crud_class.model.__tablename__
        return CallbackData(prefix, "controller", "data")

    @classmethod
    def setup(cls, dp: Dispatcher) -> None:
        handler = cls()
        dp.register_callback_query_handler(
            handler.callback_router, handler.callback_data.filter(controller="navigate_list")
        )
        dp.register_callback_query_handler(handler.callback_router, handler.callback_data.filter(controller="detail"))
        dp.register_callback_query_handler(
            handler.callback_router, handler.callback_data.filter(controller="call_action")
        )

    @classmethod
    def build_initial_callback(cls) -> str:
        prefix = cls.crud_class.model.__tablename__
        return CallbackData(prefix, "controller", "data").new(controller="navigate_list", data="1")

    async def callback_router(self, call: types.CallbackQuery, callback_data: dict) -> None:
        controller = callback_data["controller"]
        message = call.message

        if controller == "navigate_list":
            page = int(callback_data["data"])
            await self.send_list_view(message, page)

        elif controller == "detail":
            detail_id, page = callback_data["data"].split("|")
            await self.send_detail(message, int(detail_id), int(page))

        elif controller == "call_action":
            data = callback_data["data"].split("|")
            action = data[0]
            id_list = [int(val) for val in data[1].split(",")]
            payload = data[2:] if len(data) > 2 else None
            await self.call_action(action, id_list, payload, message)
        else:
            await message.answer("Неизвестная команда")

    async def initial_list_view_callback(self, call: types.CallbackQuery) -> None:
        await self.send_list_view(call.message, 1)

    async def send_list_view(self, message: Message, page: int = 1) -> None:
        async with async_session_factory() as session:
            crud = self.crud_class(session)
            items = await crud.get_list_items(current_page=page, items_per_page=self.settings.list_items_per_page)
            all_items_count = await crud.get_items_count()
        list_view_builder = self.list_view_builder(
            current_page=page,
            cancel_callback_data=self.cancel_callback_data,
            callback_data=self.callback_data,
            settings=self.settings,
            items=items,
            all_items_count=all_items_count,
        )
        await message.answer(list_view_builder.build_body(), reply_markup=list_view_builder.build_keyboard())

    async def send_detail(self, message: Message, detail_id: int, current_page: int) -> None:
        async with async_session_factory() as session:
            crud = self.crud_class(session)
            try:
                item = await crud.get_object_by_id(detail_id)
            except CRUDException as e:
                logging.error(str(e))
                await message.answer("Не удалось получить данные")
                await self.send_list_view(message, current_page)
                return
        detail_view_builder = self.detail_view_builder(
            admin_settings=self.settings,
            callback_data=self.callback_data,
            model_title=self.model_title,
            obj=item,
            actions=self.get_actions(),
            list_page=current_page,
        )
        await message.answer(detail_view_builder.build_body(), reply_markup=detail_view_builder.build_keyboard())

    async def call_action(
        self, action: str, id_list: list[int], payload: list[str] | None, message: Message
    ) -> str | None:
        if not hasattr(self, action):
            return f"Не найден метод {action}"
        admin_action = getattr(self, action)
        return await admin_action(message, id_list, payload)

    def get_actions(self) -> list[Action]:
        result = []
        for action_name in [attr for attr in dir(self) if attr.startswith("action_")]:
            action = getattr(self, action_name)
            if callable(action):
                result.append(Action(slug=action_name, description=getattr(action, "_description", "")))
        return result

    @register_action("Удалить элемент")
    async def action_remove_by_id(self, message: Message, id_list: list[int], payload: list[str] | None) -> None:
        async with async_session_factory() as session:
            crud = self.crud_class(session)
            await crud.delete_by_id(id_list)
        await message.answer("Объекты были удалены")

        page = 1
        if payload:
            try:
                page = int(payload[0])
            except ValueError:
                pass
        await self.send_list_view(message, page)
