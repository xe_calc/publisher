from io import BytesIO

from aiogram import Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.types import ContentType, Message
from sqlalchemy.exc import NoResultFound

from crud.statistics import StatisticItemViewCRUD
from handlers.admin_item_handler import BaseAdminHandler, register_action
from handlers.menu_handlers import analytic_menu
from keyboards.analyics_keyboards import build_cancel_analytic_button
from keyboards.menu_keyboards import build_analytics_menu
from models.db import async_session_factory
from services.load_product_post import LoadProductPostService
from services.load_statistics_service import (
    LoadStatisticService,
    LoadStatisticServiceException,
)
from states import LoadStatistic
from views.dto import AdminSettings
from views.renders import AnyToStrRender


async def start_analytics_load(call: types.CallbackQuery, state: FSMContext) -> None:
    await call.message.answer("Загрузите csv файл со статистикой", reply_markup=build_cancel_analytic_button())
    await state.set_state(LoadStatistic.await_load_file)


async def load_analytic_csv(message: types.Message, state: FSMContext) -> None:
    if message.content_type == ContentType.DOCUMENT:
        file_name_split = message.document.file_name.split(".")
        if len(file_name_split) > 1 and file_name_split[-1] == "csv":
            file_in_io = BytesIO()
            await message.document.download(destination_file=file_in_io)
            try:
                (
                    success_count,
                    ignored_count,
                    errors_count,
                ) = await LoadStatisticService().parse_csv_and_update_products_statistic(file_in_io)
            except LoadStatisticServiceException as e:
                await message.answer(str(e), reply_markup=build_analytics_menu())
            else:
                await message.answer(
                    f"Загрузка прошла успешно. Обработано: {success_count}, проигнорировано: {ignored_count}, "
                    f"ошибок: {errors_count}",
                    reply_markup=build_analytics_menu(),
                )
            await state.finish()
        else:
            await message.answer("У документа расширение не csv", reply_markup=build_cancel_analytic_button())
    else:
        await message.answer(
            "Нужно загрузить документ, либо нажмите отмена.", reply_markup=build_cancel_analytic_button()
        )


async def cancel_load_analytics(call: types.CallbackQuery, state: FSMContext) -> None:
    await call.message.answer("Операция отменена")
    await analytic_menu(call.message)
    await state.finish()


async def exit_analytics_list_view(call: types.CallbackQuery) -> None:
    await analytic_menu(call.message)


class StatListView(BaseAdminHandler):
    crud_class = StatisticItemViewCRUD
    settings = AdminSettings(
        list_fields=["id", "title", "view_count"], detail_scheme=["id", "title", "view_count", "has_post"]
    )
    model_title = "Элемент статистики"
    cancel_callback_data = "stat_list_cancel"

    @register_action("Создать пост")
    async def action_create_portion(self, message: Message, id_list: list[int], payload: list[str] | None) -> None:
        async with async_session_factory() as session:
            items = []
            for id_item in id_list:
                try:
                    item = await self.crud_class(session).get_object_by_id(id_item)
                    if not item.post:
                        items.append(item)
                except NoResultFound:
                    pass
            load_product_post_service = LoadProductPostService(session=session)
            results = {}
            for item in items:
                result = await load_product_post_service.load_product_post(item)
                results[item.id] = result

        response = ""
        for stat_id, result in results.items():
            response += f"{stat_id}: {AnyToStrRender.render(result)}\n"
        await message.answer(f"Результат:\n {response}")


def setup(dp: Dispatcher) -> None:
    StatListView.setup(dp)

    dp.register_callback_query_handler(start_analytics_load, lambda c: c.data == "publisher_menu_analytics_load")
    dp.register_callback_query_handler(cancel_load_analytics, lambda c: c.data == "analytics_cancel", state="*")
    dp.register_message_handler(load_analytic_csv, state=LoadStatistic.await_load_file, content_types=ContentType.ANY)

    dp.register_callback_query_handler(exit_analytics_list_view, lambda c: c.data == StatListView.cancel_callback_data)
