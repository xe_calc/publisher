from io import BytesIO

from aiogram import Dispatcher, types
from aiogram.types import Message

from content_builder.storage import LocalStorage
from crud.content_target import TargetAdminViewCRUD
from crud.prepared_content import PreparedContentAdminCRUD
from crud.product_post import ProductPostAdminCRUD
from handlers.admin_item_handler import BaseAdminHandler, register_action
from handlers.menu_handlers import publisher_root_menu
from models.content_types import get_content
from models.db import async_session_factory
from services.content_builder import PrepareContentService
from views.dto import AdminSettings


class TargetListView(BaseAdminHandler):
    crud_class = TargetAdminViewCRUD
    settings = AdminSettings(
        list_fields=["id", "title", "system_slug"],
        detail_scheme=["id", "title", "system_slug", "credentials", "slots_as_list"],
    )
    model_title = "Цель публикации"
    cancel_callback_data = "back_to_publisher_menu"


async def back_to_publisher_menu(call: types.CallbackQuery) -> None:
    # TODO - это должно быть частью BaseAdminHandler
    await publisher_root_menu(call.message)


class XeCalcPostView(BaseAdminHandler):
    crud_class = ProductPostAdminCRUD
    settings = AdminSettings(
        list_fields=["id", "title"], detail_scheme=["id", "title", "portions_list", "prepared_content_info"]
    )
    model_title = "Xecalc продукты и порции"
    cancel_callback_data = "target_list_cancel"

    @register_action("Подготовить контент из порций")
    async def action_create_portion(self, message: Message, id_list: list[int], payload: list[str] | None) -> None:
        async with async_session_factory() as session:
            for product_id in id_list:
                await PrepareContentService(session).prepare_xe_calc_portion_post(product_id)
        await message.answer("Контент подготовлен")


class PreparedContentAdminView(BaseAdminHandler):
    crud_class = PreparedContentAdminCRUD
    settings = AdminSettings(list_fields=["id", "title"], detail_scheme=["id", "title", "payload"])
    model_title = "Подготовленный контент"
    cancel_callback_data = "target_list_cancel"

    @register_action("Показать вложения")
    async def action_show_medias(self, message: Message, id_list: list[int], payload: list[str] | None) -> None:
        async with async_session_factory() as session:
            for obj_id in id_list:
                if prepared_content := await self.crud_class(session).get_object_by_id(obj_id):
                    for image_path in get_content(prepared_content).get_media():
                        with await LocalStorage(entity="xe_calc_portion_post").open(image_path) as im:
                            bio = BytesIO()
                            bio.name = image_path
                            im.save(bio)
                            bio.seek(0)
                            await message.reply_photo(photo=bio)
        await message.answer("Контент подготовлен")


def setup(dp: Dispatcher) -> None:
    TargetListView.setup(dp)
    XeCalcPostView.setup(dp)
    PreparedContentAdminView.setup(dp)

    dp.register_callback_query_handler(back_to_publisher_menu, lambda c: c.data == TargetListView.cancel_callback_data)
    dp.register_callback_query_handler(back_to_publisher_menu, lambda c: c.data == XeCalcPostView.cancel_callback_data)
    dp.register_callback_query_handler(
        back_to_publisher_menu, lambda c: c.data == PreparedContentAdminView.cancel_callback_data
    )
