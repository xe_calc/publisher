from aiogram import Dispatcher, types

from keyboards.menu_keyboards import (
    build_analytics_menu,
    build_content_plan_menu,
    build_entry_menu,
)


async def top_menu(message: types.Message) -> None:
    await message.answer(
        "Планировщик контента для проекта xecalc.ru, выберите нужный пункт", reply_markup=build_entry_menu()
    )


async def analytic_menu(message: types.Message) -> None:
    await message.answer("Сервис работы с аналитикой, выберите нужный пункт", reply_markup=build_analytics_menu())


async def publisher_root_menu(message: types.Message) -> None:
    await message.answer("Контент план", reply_markup=build_content_plan_menu())


async def root_menu(message: types.Message) -> None:
    await top_menu(message)


async def analytics_root_menu(call: types.CallbackQuery) -> None:
    await analytic_menu(call.message)


async def back_menu_handler(call: types.CallbackQuery) -> None:
    await top_menu(call.message)


async def send_publisher_root_menu(call: types.CallbackQuery) -> None:
    await publisher_root_menu(call.message)


def setup(dp: Dispatcher) -> None:
    dp.register_message_handler(root_menu, commands={"start"})
    dp.register_callback_query_handler(analytics_root_menu, lambda c: c.data == "publisher_menu_analytics")
    dp.register_callback_query_handler(back_menu_handler, lambda c: c.data == "publisher_menu_back")
    dp.register_callback_query_handler(send_publisher_root_menu, lambda c: c.data == "publisher_menu_content_planing")
