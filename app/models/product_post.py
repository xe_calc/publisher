from typing import TYPE_CHECKING, Optional

from sqlalchemy import Boolean, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from views.renders import ListObjects

from .db import Base, TimestampMixin, float_counter, str_400

if TYPE_CHECKING:
    from .content_plan import PreparedContent
    from .statistic_item import StatisticItem


class ProductPost(Base, TimestampMixin):
    __tablename__ = "product_post"

    title: Mapped[str_400]
    statistic_item_id: Mapped[int] = mapped_column(ForeignKey("statistic_item.id"))
    portions: Mapped[list["ProductPostPortion"]] = relationship(back_populates="product_post", cascade="all, delete")
    stat_item: Mapped["StatisticItem"] = relationship(back_populates="post")
    is_active: Mapped[bool] = mapped_column(Boolean, default=True)

    prepared_content: Mapped[Optional["PreparedContent"]] = relationship(
        back_populates="product_post", cascade="all, delete"
    )

    @property
    def portions_list(self) -> ListObjects:
        # TODO убрать из модели
        result = []
        for portion in self.portions:
            result.append(portion.title)
        return ListObjects(data=result)

    @property
    def prepared_content_info(self) -> bool:
        # TODO убрать из модели
        return bool(self.prepared_content)


class ProductPostPortion(Base):
    __tablename__ = "product_post_portion"

    title: Mapped[str_400]
    post_id: Mapped[int] = mapped_column(ForeignKey(ProductPost.id))
    calories: Mapped[float_counter]
    fat: Mapped[float_counter]
    protein: Mapped[float_counter]
    carbohydrates: Mapped[float_counter]
    xe_count: Mapped[float_counter]

    product_post: Mapped["ProductPost"] = relationship(back_populates="portions")
