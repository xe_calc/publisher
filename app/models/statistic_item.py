from typing import TYPE_CHECKING

from sqlalchemy.orm import Mapped, mapped_column, relationship

from .db import Base, str_300, str_400

if TYPE_CHECKING:
    from .product_post import ProductPost


class StatisticItem(Base):
    __tablename__ = "statistic_item"

    title: Mapped[str_300]
    url: Mapped[str_400]
    view_count: Mapped[int]
    slug: Mapped[str_300] = mapped_column(unique=True)
    post: Mapped["ProductPost"] = relationship(back_populates="stat_item")

    @property
    def has_post(self) -> bool:
        return bool(self.post)
