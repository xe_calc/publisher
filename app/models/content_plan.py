import datetime
from enum import Enum
from typing import TYPE_CHECKING, Any, Optional

from sqlalchemy import Boolean, ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.types import JSON

from models.db import Base, TimestampMixin, str_400
from views.renders import ListObjects

if TYPE_CHECKING:
    from .product_post import ProductPost


class TargetSystem(Enum):
    TELEGRAM = "tg"
    VK = "vk"


class Target(Base, TimestampMixin):
    __tablename__ = "content_target"

    title: Mapped[str_400]
    system_slug: Mapped[TargetSystem]
    credentials: Mapped[dict[str, str]] = mapped_column(JSON)

    slots: Mapped[list["ContentSlot"]] = relationship(back_populates="slot_target")
    contents: Mapped[list["PreparedContent"]] = relationship(back_populates="content_target")

    @property
    def slots_as_list(self) -> ListObjects:
        # TODO слой представления, должен быть не тут
        result = []
        for slot in self.slots:
            is_cycle_text = "Циклично" if slot.is_cycle else "Один раз"
            time_str = (
                f"{slot.weekly_day.name} {slot.time_of_day}"
                if slot.is_cycle
                else f"{slot.publish_date.strftime('%d.%m.Y %H:%m:%s')}"
            )
            result.append(f"{is_cycle_text} {time_str}")
        return ListObjects(data=result)


class WeekDays(Enum):
    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
    FRIDAY = 5
    SATURDAY = 6
    SUNDAY = 7


class ContentSlot(Base):
    __tablename__ = "content_slot"

    target_id: Mapped[int] = mapped_column(ForeignKey("content_target.id"))
    publish_date: Mapped[datetime.datetime | None]
    weekly_day: Mapped[WeekDays | None]
    time_of_day: Mapped[datetime.time | None]

    slot_target: Mapped[Target] = relationship(back_populates="slots")
    contents: Mapped["PreparedContent"] = relationship(back_populates="slot")

    __table_args__ = (
        UniqueConstraint("target_id", "weekly_day", "time_of_day", "publish_date", name="unique_for_target"),
    )

    @property
    def is_cycle(self) -> bool:
        return bool(self.weekly_day and self.time_of_day)


class PreparedContent(Base, TimestampMixin):
    __tablename__ = "prepared_content"

    target_id: Mapped[int] = mapped_column(ForeignKey("content_target.id"))
    slot_id: Mapped[int | None] = mapped_column(ForeignKey("content_slot.id"))
    is_published: Mapped[bool] = mapped_column(Boolean, default=False)
    payload: Mapped[dict[str, Any]] = mapped_column(JSON)

    product_post_id: Mapped[int | None] = mapped_column(ForeignKey("product_post.id"))

    content_target: Mapped[Target] = relationship(back_populates="contents")
    slot: Mapped[ContentSlot | None] = relationship(back_populates="contents")

    product_post: Mapped[Optional["ProductPost"]] = relationship(back_populates="prepared_content")

    @property
    def title(self) -> str:
        if self.payload["content_type"] == "tg-xe-calc-portion-post":
            return f"Пост XeCalc ({self.payload.get('product_name', 'Неизвестно')})"
        return "Неизвестно"
