import datetime
from typing import Annotated, AsyncGenerator

from sqlalchemy import Float, String, func
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from config import app_config

engine = create_async_engine(app_config.postgres_url, echo=True)
async_session_factory = async_sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)


str_300 = Annotated[str, mapped_column(String(300))]
str_400 = Annotated[str, mapped_column(String(400))]
float_counter = Annotated[float, mapped_column(Float(precision=4, decimal_return_scale=2))]


async def get_session() -> AsyncGenerator:
    async with async_session_factory() as session:
        yield session


class Base(DeclarativeBase):
    id: Mapped[int] = mapped_column(primary_key=True)


class TimestampMixin:
    created_at: Mapped[datetime.datetime] = mapped_column(server_default=func.CURRENT_TIMESTAMP())
    updated_at: Mapped[datetime.datetime] = mapped_column(
        server_onupdate=func.CURRENT_TIMESTAMP(), server_default=func.CURRENT_TIMESTAMP()
    )
