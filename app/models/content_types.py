from dataclasses import dataclass
from enum import Enum

from models.content_plan import PreparedContent


class ContentType(Enum):
    TG_XE_CALC_PORTION_POST = "tg-xe-calc-portion-post"


@dataclass
class BasePost:
    content_type: ContentType

    def get_media(self) -> list[str]:
        raise NotImplementedError


@dataclass
class XeCalcPortion:
    image: str


@dataclass
class XeCalcProductPost(BasePost):
    product_name: str
    portions: list[XeCalcPortion]

    def get_media(self) -> list[str]:
        result = []
        for portion in self.portions:
            result.append(portion.image)
        return result


def get_content(content: PreparedContent) -> BasePost:
    data = content.payload
    ct = ContentType(data.get("content_type"))

    if ct == ContentType.TG_XE_CALC_PORTION_POST:
        return XeCalcProductPost(
            product_name=data.get("product_name"),
            portions=[XeCalcPortion(**por) for por in data.get("portions")],
            content_type=ct,
        )
    else:
        raise ValueError()
